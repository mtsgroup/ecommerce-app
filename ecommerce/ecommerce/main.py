from celery import Celery
from fastapi import APIRouter, FastAPI
from ecommerce.core import config
from ecommerce.core.api import api_router
from pathlib import Path
from fastapi.templating import Jinja2Templates

description = """
Ecommerce API

## Users

You will be able to:

* **Create users** 
* **Read users** 
"""

root_router  = APIRouter()
ecommerce  = FastAPI(
    title="Ecommerceapp ",
    description=description,
    version="0.0.1",
    terms_of_service="http://example.com/terms/",
    contact={
        "name": "Ngatcheu Fabrice",
        "url": "http://x-force.example.com/contact/",
    },
    license_info={
        "name": "Apache 2.0",
        "url": "https://www.apache.org/licenses/LICENSE-2.0.html",
    },
)


celery = Celery(
    __name__,
    broker=f"redis://{config.REDIS_HOST}:{config.REDIS_PORT}/{config.REDIS_DB}",
    backend=f"redis://{config.REDIS_HOST}:{config.REDIS_PORT}/{config.REDIS_DB}"
)
celery.conf.imports = [
    'ecommerce.orders.tasks',
]

BASE_PATH = Path(__file__).resolve().parent
TEMPLATES = Jinja2Templates(directory=str(BASE_PATH / "templates"))




ecommerce.include_router(root_router)
ecommerce.include_router(api_router)

# 5
if __name__ == "__main__":
    # Use this for debugging purposes only
    import uvicorn
    uvicorn.run(ecommerce, host="0.0.0.0", port=8001, log_level="debug")